<!-- Button trigger modal -->
<button class="pull-right" data-toggle="modal" data-target="#exampleModal">
    <i class="fa fa-download fa-lg"></i>
</button>
{!! Form::open(['url'=>url('export')]) !!}
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Export</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <input type="checkbox" name="field[title]" id="title">
                    <label for="defaultUnchecked">Title</label>
                </div>
                <div>
                    <input type="checkbox" name="field[author]"  id="author">
                    <label for="defaultUnchecked">Author</label>
                </div>
                <select class="browser-default custom-select" name="format">
                    <option selected>Format</option>
                    <option value="csv">CSV</option>
                    <option value="xml">XML</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Download</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<?php

namespace App\Http\Controllers;

use App\DataTables\BookDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatebookRequest;
use App\Http\Requests\UpdatebookRequest;
use App\Repositories\BookRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Exports\BookExport;
use App\Models\Book;
use Spatie\ArrayToXml\ArrayToXml;

class BookController extends AppBaseController
{
    /** @var  BookRepository */
    private $bookRepository;

    public function __construct(BookRepository $bookRepo)
    {
        $this->bookRepository = $bookRepo;
    }

    /**
     * Display a listing of the book.
     *
     * @param BookDataTable $bookDataTable
     * @return Response
     */
    public function index(BookDataTable $bookDataTable)
    {
        return $bookDataTable->render('books.index');
    }

    /**
     * Show the form for creating a new book.
     *
     * @return Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created book in storage.
     *
     * @param CreatebookRequest $request
     *
     * @return Response
     */
    public function store(CreatebookRequest $request)
    {
        $input = $request->all();

        $book = $this->bookRepository->create($input);

        Flash::success('Book saved successfully.');

        return redirect(route('books.index'));
    }

    /**
     * Display the specified book.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $book = $this->bookRepository->find($id);

        if (empty($book)) {
            Flash::error('Book not found');

            return redirect(route('books.index'));
        }

        return view('books.show')->with('book', $book);
    }

    /**
     * Show the form for editing the specified book.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $book = $this->bookRepository->find($id);

        if (empty($book)) {
            Flash::error('Book not found');

            return redirect(route('books.index'));
        }

        return view('books.edit')->with('book', $book);
    }

    /**
     * Update the specified book in storage.
     *
     * @param  int              $id
     * @param UpdatebookRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatebookRequest $request)
    {
        $book = $this->bookRepository->find($id);

        if (empty($book)) {
            Flash::error('Book not found');

            return redirect(route('books.index'));
        }

        $book = $this->bookRepository->update($request->all(), $id);

        Flash::success('Book updated successfully.');

        return redirect(route('books.index'));
    }

    /**
     * Remove the specified book from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $book = $this->bookRepository->find($id);

        if (empty($book)) {
            Flash::error('Book not found');

            return redirect(route('books.index'));
        }

        $this->bookRepository->delete($id);

        Flash::success('Book deleted successfully.');

        return redirect(route('books.index'));
    }

    /**
     * Export request
     */
    public function export()
    {
        $fields = request('field');
        $format = request('format');

        if (!$fields || !$format) {
            Flash::error('Invalid inputs');

            return redirect(route('books.index'));
        }
        $fields = array_keys(request('field'));
        if ($format === 'csv') {
            $filename = time().'.xlsx';
            return (new BookExport($fields))->download($filename);
        } else {
            return $this->downloadXML($fields);
         }
    }

    /**
     * Down load XML file
     */
    private function downloadXML($fields)
    {
        $modelArray = Book::select($fields)->get()->groupBy('title')->toArray();
        $xml = ArrayToXml::convert($modelArray);
        $filename = time().'.xml';
        \File::put(public_path($filename),$xml);
        return response()->download(public_path($filename), $filename, [
            'Content-Type'=>"text/xml"
        ]);
    }
}

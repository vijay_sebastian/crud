<?php

namespace App\Exports;

use App\Models\Book;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BookExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct(array $select)
    {
        $this->select = $select;
    }

    public function query()
    {
        return Book::query()->select($this->select);
    }

    public function headings(): array
    {
        return [
            array_map('strtoupper', $this->select)
        ];
    }
}

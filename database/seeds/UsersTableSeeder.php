<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        App\User::create([
            'name' => 'Test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}

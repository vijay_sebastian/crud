# Books

This a small application to do basic CRUD process.

Here we are trying to manage a book stall system.

# System requirement

 1. PHP >= 7.1.3
 2. OpenSSL PHP Extension
 3. PDO PHP Extension
 4. Mbstring PHP Extension
 5. Tokenizer PHP Extension
 6. XML PHP Extension
 7. Ctype PHP Extension
 8. JSON PHP Extension
 9. BCMath PHP Extension

# How to install

 1. Clon/download the application source code
 2. run composer update
 3. run php artisan migrate
 4. run php artisan db:seed

# Default username and password

 1. username : test@gmail.com
 2. password : secret

# Run the unit test cases

 ./vendor/bin/phpunit
